from pexpect import pxssh
class Bot:

    def __init__(self, host, user, password) -> None:
        self.host = host
        self.user = user
        self.password = password
        self.is_connected = False
        self.ssh_connection = None

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, value):
        self._host = value

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, value):
        self._user = value

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        self._password = value

    @property
    def is_connected(self):
        return self._is_connected

    @is_connected.setter
    def is_connected(self, value):
        self._is_connected = value

    @property
    def ssh_connection(self):
        return self._ssh_connection

    @ssh_connection.setter
    def ssh_connection(self, value):
        self._ssh_connection = value

    def exec_command(self, command: str) -> None:
        print(f'Executando comando .... no host {self.host}')
        self.ssh_connection.sendline(command)
        self.ssh_connection.prompt() 
        print(self.ssh_connection.before.decode())

    def connect(self) -> None:
        try:
            self.ssh_connection = pxssh.pxssh()
            self.ssh_connection.login(self.host, self.user, self.password)
            self.is_connected = True
        except:
            print(f'Cant connect host {self.host}')
            return None
